﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Midi;
    
namespace MidiDotNetTest
{
    class Program
    {
        static OutputDevice Keyboard = OutputDevice.InstalledDevices[1];
        static InputDevice inputDevice = InputDevice.InstalledDevices[0];
        static void Main(string[] args)
        {
            
            inputDevice.Open();
            inputDevice.NoteOn += new InputDevice.NoteOnHandler(NoteOn);
            inputDevice.ProgramChange += new InputDevice.ProgramChangeHandler(ProgramChange);
            
            inputDevice.ControlChange += new InputDevice.ControlChangeHandler(ControlChange);
            inputDevice.StartReceiving(null);  // Note events will be received in another thread
            //inputDevice.SysEx += new SysExHandler(SysExMessage msg);
            Console.ReadKey();  // This thread waits for a keypress
            
            Keyboard.Open();
            Keyboard.Si
            Keyboard.SendNoteOn(Channel.Channel1, Pitch.C5, 100);
            Keyboard.SendControlChange(Channel.Channel1, Control.SustainPedal, 127);
            Keyboard.SendNoteOff(Channel.Channel1, Pitch.C5, 100);
            Keyboard.SendProgramChange(Channel.Channel1, Instrument.ChurchOrgan);
            note(Pitch.C5);
            note(Pitch.C4);
            Console.ReadKey();
            Keyboard.SendControlChange(Channel.Channel1, Control.SustainPedal, 20);
            note(Pitch.C2);

            Console.ReadKey();
            Keyboard.Close();
        }

        private static void ControlChange(ControlChangeMessage msg)
        {
            Console.WriteLine("Control change: " + msg);
        }

        private static void ProgramChange(ProgramChangeMessage msg)
        {
            Console.WriteLine("Program change: " + msg);
        }

        public static void NoteOn(NoteOnMessage msg)
        {
            Console.WriteLine(msg.Pitch.NotePreferringSharps());
        }
        public static void note(Pitch pitchObject)
        {
            
            Keyboard.SendNoteOn(Channel.Channel1, pitchObject, 100);
            Keyboard.SendNoteOff(Channel.Channel1, pitchObject, 100);
        }
    }
}
