**# README #**

### Description ###

Testing the Midi.NET library with a midi keyboard to change settings and play sounds from the keyboard.


### Requirements ###

This project uses the Midi Dot Net library.
https://www.nuget.org/packages/midi-dot-net/


### 
Christian Safka

christiansafka@gmail.com